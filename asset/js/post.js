$(document).ready(function () {
	$("#persen_uang").val((eval($("#uang_dp_persen").val()) * eval($("#harga_jual").val())) / 100);

	$("#uang_dp_persen").change(function(){
		var key = newText($(this).val());
		var harga_jual = $("#harga_jual").val();
		var total = (eval(key) * eval(harga_jual)) / 100;
		//$(".persen_dp").html(total);
		$(".persen_dp").html(total).formatCurrency({
			//colorize: true,
			//decimalSymbol: '',
			//digitGroupSymbol: ',',
			roundToDecimalPlace: 2,
			symbol: ''
		});
		$("#persen_uang").val(total);
	});
	
	$("#btn_calculation").click(function () {
		//console.log('tes');
		var harga_jual	= $("#harga_jual").val();
		var persen_uang	= $("#persen_uang").val();

		if ($("#masa").val() == "") {
			var masa = "all_masa";
		} else {
			var masa = $("#masa").val();
		}

		if ($("#leasing").val() == "") {
			var leasing = "all_leasing";
		} else {
			var leasing = $("#leasing").val();
		}

		$.ajax({
			type: "POST",
			global: true,
			async: true,
			cache: false,
			dataType: "html",
			url: base_url + "post/get_leasing",
			data: {
				harga_jual: harga_jual,
				persen_uang: persen_uang,
				masa: masa,
				leasing: leasing
				},

			beforeSend: function() {
				$("#loading_data").html('<img src="' + base_url + 'images/loading.gif" style="width: 40%;" />');
			}, 

			success: function (response) {
				$("#loading_data").html(response);
			}
		});
	});
});

function newText(text) {
	if (text == '' || text == undefined) {
		return 0;
	} else {
		var regex = /,/g;
		var replaceWith = '';
		return text.replace(regex, replaceWith);
	}
}

function sep_uang(selector) {
	$(selector).formatCurrency({
		symbol: '',
		decimalSymbol: '.',
		digitGroupSymbol: ',',
		roundToDecimalPlace: 2,
		colorize: true
	});
}