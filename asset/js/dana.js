$(document).ready(function(){
	$(".penerimaandana").click(function(){
		$(".penerimaandana").attr("disabled", true);
		$(".penerimaandana").html("Sedang Proses...");
		// validasi semua form
		if($("#pemerintah").val() == 0) {
			$(".pemerintah").html("Sumber Dana Harus Diisi");
			$(".pemerintah").fadeOut(10000);
			$("#pemerintah").focus();
			$(".penerimaandana").removeAttr("disabled");
			$(".penerimaandana").html("Proses");
			return false;
		} else if($("#sumberdana").val() == "") {
			$(".sumberdana").html("Sumber Dana Harus Diisi");
			$(".sumberdana").fadeOut(10000);
			$("#sumberdana").focus();
			$(".penerimaandana").removeAttr("disabled");
			$(".penerimaandana").html("Proses");
			return false;
		} else if($("#nominaldana").val() == "") {
			$(".nominaldana").html("Nominal Dana Harus Diisi");
			$(".nominaldana").fadeOut(10000);
			$("#nominaldana").focus();
			$(".penerimaandana").removeAttr("disabled");
			$(".penerimaandana").html("Proses");
			return false;
		} else if($("#penerima_dana").val() == "") {
			$(".penerima_dana").html("Penerima Dana Harus Diisi");
			$(".penerima_dana").fadeOut(10000);
			$("#penerima_dana").focus();
			$(".penerimaandana").removeAttr("disabled");
			$(".penerimaandana").html("Proses");
			return false;
		} else {
			$("#frmpenerimaandana").submit();
		}
	});

	if($("#status").val() == "edit"){
		ajax_barang($("#sumberdana").val(), $("#pengeluarandanaid").val());
	}

	$(".pengeluarandana").click(function(){
		$(".pengeluarandana").attr("disabled", true);
		$(".pengeluarandana").html("Sedang Proses...");
		// validasi semua form
		if(parseFloat(newText($("#nominaldana").val())) > parseFloat(eval($("#jumlah_sisa").val()) + eval($("#jumlah").val()))) {
			alert("Jumlah Dana tidak Mencukupi, sisa (Rp. "+ parseFloat(eval($("#jumlah_sisa").val()) + eval($("#jumlah").val())) +")");
			$("#nominaldana").focus();
			$(".barangkeluar").removeAttr("disabled");
			$(".barangkeluar").html("Proses");
			return false;
		}
		if($("#sumberdana").val() == "") {
			$(".sumberdana").html("Sumber Dana Harus Diisi");
			$(".sumberdana").fadeOut(10000);
			$("#sumberdana").focus();
			$(".pengeluarandana").removeAttr("disabled");
			$(".pengeluarandana").html("Proses");
			return false;
		} else if($("#keperluandana").val() == "") {
			$(".keperluandana").html("Keperluan Dana Harus Diisi");
			$(".keperluandana").fadeOut(10000);
			$("#keperluandana").focus();
			$(".pengeluarandana").removeAttr("disabled");
			$(".pengeluarandana").html("Proses");
			return false;
		} else if($("#nominaldana").val() == "") {
			$(".nominaldana").html("Nominal Dana Harus Diisi");
			$(".nominaldana").fadeOut(10000);
			$("#nominaldana").focus();
			$(".pengeluarandana").removeAttr("disabled");
			$(".pengeluarandana").html("Proses");
			return false;
		} else {
			$("#frmpengeluarandana").submit();
		}
	});

	$("#sumberdana").change(function(){
		var kode_dana = $(this).val();
		var pengeluarandanaid = $("#pengeluarandanaid").val();
		ajax_barang(kode_dana, pengeluarandanaid);
	});
});

function ajax_barang(kode_dana, pengeluarandanaid = ""){
	$.ajax({
		url: base_url + "pengeluaran_danas/get_jumlah_dana",
		type: "POST",
		data: { 
			kode_dana : kode_dana,
			pengeluarandanaid : pengeluarandanaid,
			status_action : $("#status").val()
			},
		dataType: "json",
		cache: false,
		beforeSend: function() {
			$(".jumlah_sisa").html('<img src="' + base_url + 'images/loader.gif"/>');
		},
		success: function(response){
			$(".jumlah_sisa").html("Tersisa : Rp. "+response.jumlah_dana_show);
			$("#jumlah_sisa").val(response.jumlah_dana);
			if(response.status_action == "edit"){
				$(".jumlah").html("+ Rp. "+response.jumlah_dana_keluar_show);
				$("#jumlah").val(response.jumlah_dana_keluar);
			}
		}
	});
}

function newText(text) {
	if (text == '' || text == undefined) {
		return 0;
	} else {
		var regex = /,/g;
		var replaceWith = '';
		return text.replace(regex, replaceWith);
	}
}