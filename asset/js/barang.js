$(document).ready(function(){
	$(".barangmasuk").click(function(){
		$(".barangmasuk").attr("disabled", true);
		$(".barangmasuk").html("Sedang Proses...");
		// validasi semua form
		if($("#namabarang").val() == "") {
			$(".namabarang").html("Nama Barang Harus Diisi");
			$(".namabarang").fadeOut(10000);
			$("#namabarang").focus();
			$(".barangmasuk").removeAttr("disabled");
			$(".barangmasuk").html("Proses");
			return false;
		} else if($("#jenisbarang").val() == "") {
			$(".jenisbarang").html("Jenis Barang Harus Diisi");
			$(".jenisbarang").fadeOut(10000);
			$("#jenisbarang").focus();
			$(".barangmasuk").removeAttr("disabled");
			$(".barangmasuk").html("Proses");
			return false;
		} else if($("#jumlahbarang").val() == "") {
			$(".jumlahbarang").html("Jumlah Barang Harus Diisi");
			$(".jumlahbarang").fadeOut(10000);
			$("#jumlahbarang").focus();
			$(".barangmasuk").removeAttr("disabled");
			$(".barangmasuk").html("Proses");
			return false;
		} else if($("#satuan_barang").val() == "") {
			$(".satuan_barang").html("Satuan Barang Harus Diisi");
			$(".satuan_barang").fadeOut(10000);
			$("#satuan_barang").focus();
			$(".barangmasuk").removeAttr("disabled");
			$(".barangmasuk").html("Proses");
			return false;
		} else if($("#pemerintah").val() == 0) {
			$(".pemerintah").html("Sumber Barang Harus Diisi");
			$(".pemerintah").fadeOut(10000);
			$("#pemerintah").focus();
			$(".barangmasuk").removeAttr("disabled");
			$(".barangmasuk").html("Proses");
			return false;
		} else if($("#sumberbarang").val() == "") {
			$(".sumberbarang").html("Sumber Barang Harus Diisi");
			$(".sumberbarang").fadeOut(10000);
			$("#sumberbarang").focus();
			$(".barangmasuk").removeAttr("disabled");
			$(".barangmasuk").html("Proses");
			return false;
		} else {
			$("#frmbarangmasuk").submit();
		}
	});

	if($("#status").val() == "edit"){
		ajax_barang($("#namabarang").val(), $("#barangkeluarid").val());
	}

	$(".barangkeluar").click(function(){
		$(".barangkeluar").attr("disabled", true);
		$(".barangkeluar").html("Sedang Proses...");
		// validasi semua form
		if(parseFloat(newText($("#jumlahbarang").val())) > parseFloat(eval($("#jumlah_sisa").val()) + eval($("#jumlah").val()))) {
			alert("Jumlah Barang tidak Mencukupi, sisa ("+ parseFloat(eval($("#jumlah_sisa").val()) + eval($("#jumlah").val())) +" Unit)");
			$("#jumlahbarang").focus();
			$(".barangkeluar").removeAttr("disabled");
			$(".barangkeluar").html("Proses");
			return false;
		}
		if($("#namabarang").val() == "") {
			$(".namabarang").html("Nama Barang Harus Diisi");
			$(".namabarang").fadeOut(10000);
			$("#namabarang").focus();
			$(".barangkeluar").removeAttr("disabled");
			$(".barangkeluar").html("Proses");
			return false;
		} else if($("#jumlahbarang").val() == "") {
			$(".jumlahbarang").html("Jumlah Barang Harus Diisi");
			$(".jumlahbarang").fadeOut(10000);
			$("#jumlahbarang").focus();
			$(".barangkeluar").removeAttr("disabled");
			$(".barangkeluar").html("Proses");
			return false;
		} else if($("#keperluanbarang").val() == "") {
			$(".keperluanbarang").html("Keperluan Barang Harus Diisi");
			$(".keperluanbarang").fadeOut(10000);
			$("#keperluanbarang").focus();
			$(".barangkeluar").removeAttr("disabled");
			$(".barangkeluar").html("Proses");
			return false;
		} else {
			$("#frmbarangkeluar").submit();
		}
	});

	$("#namabarang").change(function(){
		var kode_barang = $(this).val();
		var barangkeluarid = $("#barangkeluarid").val();
		ajax_barang(kode_barang, barangkeluarid);
	});
});

function ajax_barang(kode_barang, barangkeluarid = ""){
	$.ajax({
		url: base_url + "barang_keluars/get_jumlah_barang",
		type: "POST",
		data: { 
			kode_barang : kode_barang,
			barangkeluarid : barangkeluarid,
			status_action : $("#status").val()
			},
		dataType: "json",
		cache: false,
		beforeSend: function() {
			$(".jumlah_sisa").html('<img src="' + base_url + 'images/loader.gif"/>');
		},
		success: function(response){
			$(".jumlah_sisa").html("Tersisa : "+response.jumlah_brg_show +" Unit");
			$("#jumlah_sisa").val(response.jumlah_brg);
			if(response.status_action == "edit"){
				$(".jumlah").html("+ "+response.jumlah_brg_keluar_show +" Unit");
				$("#jumlah").val(response.jumlah_brg_keluar);
			}
		}
	});
}

function newText(text) {
	if (text == '' || text == undefined) {
		return 0;
	} else {
		var regex = /,/g;
		var replaceWith = '';
		return text.replace(regex, replaceWith);
	}
}