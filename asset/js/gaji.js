$(document).ready(function(){
  $(".info_").hide();
	$("#user").change(function(){
        var user = $(this).val();
        $.ajax({
            url: base_url + "gajis/cari_user",
            type: "POST",
            data: { userid : user },
            dataType: "json",
            cache: false,
            success: function(respon){
            	var response = respon.gaji;
              var respon_info = respon.info;
         			$("#userid").val(response.userid);
         			$("#gaji_pokok").val(response.gaji_pokok);
         			$("#tunj_pasangan").val(response.tunj_pasangan);
         			$("#tunj_anak").val(response.tunj_anak);
         			$("#tunj_umum").val(response.tunj_umum);
         			$("#tunj_jabatan_struktural").val(response.tunj_jabatan_struktural);
         			$("#tunj_fungsional").val(response.tunj_fungsional);
         			$("#tunj_beras").val(response.tunj_beras);
         			$("#tunj_pajak").val(response.tunj_pajak);
         			$("#pot_iuran_wajib").val(response.pot_iuran_wajib);
         			$("#pot_pajak").val(response.pot_pajak);
         			$("#pot_sewa_rumah").val(response.pot_sewa_rumah);
         			$("#pot_taperum").val(response.pot_taperum);
         			$("#pot_iuran_koperasi").val(response.pot_iuran_koperasi);
         			$("#pot_arisan").val(response.pot_arisan);
         			$("#pot_kredit").val(response.pot_kredit);
         			$(".jumlah_tunjangan").html(respon.jumlah_tunjangan);
         			$(".jumlah_potongan").html(respon.jumlah_potongan);
         			$(".total").html(respon.total);

              $(".info_").show();
              $(".info_nip").html(respon_info.nip);
              $(".info_kawin").html(respon_info.statuskawin);
              $(".info_anak").html(respon_info.jumlah_anak + " Anak");
              $(".info_golongan").html(respon_info.golongan_nama);
              $(".info_jabatan").html(respon_info.jabatan_nama);
            },
            error: function(jqXHR, textStatus, errorThrown) {
		        console.log(textStatus); //error logging
		    }
        });

    });
});
