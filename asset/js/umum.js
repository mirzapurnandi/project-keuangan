$(document).ready(function(){
	// Mendapatkan panel kiri
	load_kategori();
	$("#merk").change(function(){
		var merkid = $(this).val();
		$.ajax({
			url: base_url + "ajax_function/get_model",
			type: "POST",
			data: { merkid : merkid },
			dataType: "html",
			cache: false,
			success: function(response){
				$("#model").html(response);
			}
		});
	});

	$(".jumlah").html(per_page);
	$(".loading_data").click(function() {
		var jumlah = $("#jumlah").val();
		var list_grid = $("#list_grid").val();

		$.ajax({
			type: "POST",
			global: true,
			async: true,
			cache: false,
			dataType: "html",
			url: base_url + "utama/get_loading_data",
			data: {
					jumlah: jumlah,
					list_grid: list_grid
				},

			beforeSend: function() {
				$(".loading_data").hide();
				$(".loading_data_show").html('<img src="' + base_url + 'images/loading.gif" style="width: 50%;" />');
			},

			success: function (response) {
				$("ul."+list_grid+"").append(response);
				$(".loading_data_show").hide();
				$(".loading_data").show();
				$("#jumlah").val(eval(jumlah) + eval(per_page));
				$(".jumlah").html(eval(jumlah) + eval(per_page));

				if($("#jumlah").val() >= $("#count_data").val()){
					$(".loading_data").hide();
				}
			}
		});
	});
});

function load_kategori() {
	$.ajax({
		url: base_url + "utama/get_kategori",
		global: true,
		type: "POST",
		async: true,
		dataType: "html",
		success: function (response) {
			$(".list_kategori").html(response);
		},
		complete: function () {
			load_berita();
		},
		beforeSend: function () {
			$(".list_kategori").html('<div id="loading" align="center"><img src="' + base_url + 'images/loading2.gif" width="100px"/></div>');
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$(".list_kategori").html("")
		}
	});
}

function load_berita() {
	$.ajax({
		url: base_url + "utama/get_berita",
		global: true,
		type: "POST",
		async: true,
		dataType: "html",
		success: function (response) {
			$(".list_berita").html(response);
		},
		 complete: function () {
			load_populer();
		},
		beforeSend: function () {
			$(".list_berita").html('<div id="loading" align="center"><img src="' + base_url + 'images/loading2.gif" width="100px"/></div>');
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$(".list_berita").html("")
		}
	});
}

function load_populer() {
	$.ajax({
		url: base_url + "utama/get_populer",
		global: true,
		type: "POST",
		async: true,
		dataType: "html",
		success: function (response) {
			$(".list_populer").html(response);
		},
		/* complete: function () {
			list_po_masa_berlaku();
		}, */
		beforeSend: function () {
			$(".list_populer").html('<div id="loading" align="center"><img src="' + base_url + 'images/loading2.gif" width="100px"/></div>');
		},
		error: function (xhr, ajaxOptions, thrownError) {
			$(".list_populer").html("")
		}
	});
}