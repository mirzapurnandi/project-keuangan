<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Bidang extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get_data($id = ''){
		$this->db->select('
			bidang.*
		');
		$this->db->from('bidang');
		if($id != ""){
			$this->db->where('bidang.id', $id);
		}
		$this->db->order_by('bidang.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}
}
?>
