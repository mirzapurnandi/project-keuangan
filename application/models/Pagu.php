<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Pagu extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get_data($id = ''){
		$this->db->select('
			pagu.*
		');
		$this->db->from('pagu');
		if($id != ""){
			$this->db->where('pagu.id', $id);
		}
		$this->db->order_by('pagu.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function cek_data($kode){
		$this->db->select('
			pagu.id
		');
		$this->db->from('pagu');
		$this->db->where('pagu.kode', $kode);
		$query = $this->db->get();
		return $query->num_rows();
	}
}
?>
