<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Satuan_barang extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get_data($id = ''){
		$this->db->select('
			satuan_barang.*
		');
		$this->db->from('satuan_barang');
		if($id != ""){
			$this->db->where('satuan_barang.id', $id);
		}
		$this->db->order_by('satuan_barang.id', 'DESC');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}
}
?>
