<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class User extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get_data($id = ""){
		$this->db->select('
			user.*,
			IF(user.jk = 1, "Laki-laki", "Wanita") as jenis_kelamin,
			login.username,
			login.level
		');
		$this->db->from('user');
		$this->db->join('login', 'login.userid = user.id', 'left');

		if($id != ""){
			$this->db->where('user.id', $id);
		}
		$this->db->order_by('user.id', 'asc');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function get_data_login($id = ""){
		$this->db->select('
			login.*
		');
		$this->db->from('login');

		if($id != ""){
			$this->db->where('login.id', $id);
		}
		$this->db->order_by('login.id', 'asc');

		$query = $this->db->get();
		if($query->num_rows() > 0){
			if($id == ""){
				return $query->result_array();
			} else {
				return $query->row_array();
			}
		} else {
			return NULL;
		}
	}

	function utama($id){
		$this->db->select('
			IFNULL(user.nama, "-") as nama, 
			IFNULL(user.nip, "-") as nip
		');
		$this->db->where('user.id', $id);
		$query = $this->db->get('user');
		if($query->num_rows() > 0){
			return $query->row();
		} else {
			return NULL;
		}
	}
}
?>
