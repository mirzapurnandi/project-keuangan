<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Setting extends CI_Model {

	function __construct() {
		parent::__construct();
	}

	function get_data(){
		$this->db->select('
			setting.*
		');
		$this->db->from('setting');
		$this->db->where('setting.id', config_item('set_id'));
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row_array();
		} else {
			return NULL;
		}
	}
}
?>
