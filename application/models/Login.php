<?php
if(!defined('BASEPATH'))
	exit('No direct script access allowed');

class Login extends CI_Model {

	function __construct() {
		parent::__construct();
		date_default_timezone_set("Asia/Jakarta");
	}

	public function cek_user($data){
		$query = $this->db->get_where('login',$data);
		return $query;
	}

	function ambil($username = ""){
		$this->db->select('
			login.*
		');
		$this->db->from('login');
		if($username != ""){
			$this->db->where('username', $username);
		}
		$login = $this->db->get();
		if($login->num_rows() > 0){
			return $login->result_array();
		} else {
			return NULL;
		}
	}
	
}
?>
