<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Daftar Username Pegawai
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url();?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li>Username Pegawai</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<!-- SELECT2 EXAMPLE -->
	<div class="box box-default">
		<?php if($this->session->userdata('level') == "admin"){?>
			<div class="box-header with-border col-md-2">
				<a href="<?= site_url('user_logins/add')?>" class="btn btn-block btn-primary btn-lg">Tambah</a>
			</div>
		<?php } ?>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<font class="info"><?=$this->session->flashdata('pesan');?></font>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th width="10%">No</th>
							<th width="50%">Username</th>
							<th width="30%">Level</th>
							<th width="10%">Actions</th>
						</tr>
						</thead>
						<tbody>
						<?php
						if(count($result) > 0){
						foreach($result as $key => $val){ ?>
						<tr>
							<td><?= $key + 1; ?></td>
							<td><?= $val['username'];?></td>
							<td><?= $val['level']; ?></td>
							<td>
								<?php if($this->session->userdata('level') == "admin"){
									if($val['id'] != '1') { ?>
									<a href="<?= site_url('user_logins/edit/'.$val['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
								
									<a href="<?= site_url('user_logins/remove/'.$val['id']); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Mau Menghapus Data ini... ?')"><span class="fa fa-trash"></span> Delete</a>
								<?php } } ?>
							</td>
						</tr>
						<?php } }?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			Informasi seluruh Username Pegawai.
		</div>
	</div>
	<!-- /.box -->
	</section>
</div>