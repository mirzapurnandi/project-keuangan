<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Data Username Pegawai</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?= site_url('users')?>">Data Username Pegawai</a></li>
			<li class="active">Tambah Username Pegawai</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Username</h3>
					</div>
					<form role="form" action="<?=site_url('user_logins/proses'); ?>" method="post">
						<div class="modal-body">
							<div class="box-body">
								<div class="form-group">
									<label for="bidang">Nama Pegawai <font color="red">*</font></label>
									<?= form_dropdown('user', $user, '', 'id="user" class="form-control" required'); ?>
								</div>
								<div class="form-group">
									<label for="nama">Username <font color="red">*</font></label>
									<input type="text" class="form-control" id="username" name="username" placeholder="Masukan Username Pegawai" required>
									<input type="hidden" class="form-control" name="loginid" value="">
								</div>
								<div class="form-group">
									<label for="nip">Password</label>
									<input type="text" class="form-control" id="password" name="password" placeholder="Masukan Password">
								</div>
								<div class="form-group">
									<label for="bidang">Level <font color="red">*</font></label>
									<?= form_dropdown('level', $level, '', 'id="level" class="form-control" required'); ?>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>