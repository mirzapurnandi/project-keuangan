<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Data Pegawai</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="<?= site_url('users')?>">Data Pegawai</a></li>
			<li class="active">Tambah Pegawai</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Tambah Data</h3>
					</div>
					<form role="form" action="<?=site_url('users/proses'); ?>" method="post">
						<div class="modal-body">
							<div class="box-body">
								<div class="form-group">
									<label for="nama">Nama <font color="red">*</font></label>
									<input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Nama Pegawai" required>
									<input type="hidden" class="form-control" name="userid" value="">
								</div>
								<div class="form-group">
									<label for="nip">NIP</label>
									<input type="text" class="form-control" id="nip" name="nip" placeholder="Masukan NIP">
								</div>
								<div class="form-group">
									<label for="no_hp">No HP</label>
									<input type="text" class="form-control" id="no_hp" name="no_hp" placeholder="Masukan No HP">
								</div>
								<div class="form-group">
									<label for="bidang">Bidang <font color="red">*</font></label>
									<?= form_dropdown('bidang', $bidang, '', 'id="bidang" class="form-control" required'); ?>
								</div>
								<div class="form-group">
									<label for="jenis_kelamin">Jenis Kelamin</label>
									<?= form_dropdown('jenis_kelamin', $jenis_kelamin, '', 'id="jenis_kelamin" class="form-control"'); ?>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>