<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Daftar Pegawai
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url();?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li>User</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<!-- SELECT2 EXAMPLE -->
	<div class="box box-default">
		<?php if($this->session->userdata('level') == "admin"){?>
			<div class="box-header with-border col-md-2">
				<a href="<?= site_url('users/add')?>" class="btn btn-block btn-primary btn-lg">Tambah</a>
			</div>
		<?php } ?>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<font class="info"><?=$this->session->flashdata('pesan');?></font>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th width="10%">No</th>
							<th width="40%">Nama / NIP</th>
							<th width="20%">Username</th>
							<th width="20%">Level</th>
							<th width="10%">Actions</th>
						</tr>
						</thead>
						<tbody>
						<?php
						if(count($result) > 0){
						foreach($result as $key => $val){ ?>
						<tr>
							<td><?= $key + 1; ?></td>
							<td><?= $val['nama'] .'<br>'.$val['nip'];?></td>
							<td><?= $val['username'] == "" ? '-masih kosong-' : $val['username']; ?></td>
							<td><?= $val['level'] == "" ? "-" : $val['level']; ?></td>
							<td>
								<a href="<?= site_url('users/edit/'.$val['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
								<?php if($this->session->userdata('level') == "admin"){ ?>
									<a href="<?= site_url('users/remove/'.$val['id']); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Mau Menghapus Data ini... ?')"><span class="fa fa-trash"></span> Delete</a>
								<?php } ?>
							</td>
						</tr>
						<?php } }?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			Informasi seluruh Pegawai.
		</div>
	</div>
	<!-- /.box -->
	</section>
</div>