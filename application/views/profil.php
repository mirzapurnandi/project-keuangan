<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1> User Profile </h1>
		<ol class="breadcrumb">
		<li><a href="<?= site_url()?>"><i class="fa fa-dashboard"></i> Home</a></li>
		<li class="active">User profile</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">

		<div class="row">
			<div class="col-md-3">
				<!-- Profile Image -->
				<div class="box box-primary">
					<div class="box-body box-profile">
						<img class="profile-user-img img-responsive img-circle" src="<?= config_item('base_url')?>images/user.png" alt="User profile picture">
						<h3 class="profile-username text-center"><?=$result['nama']?></h3>
						<p class="text-muted text-center"><?=$result['nip']?></p>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
			<div class="col-md-9">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#activity" data-toggle="tab">Informasi Data</a></li>
						<li><a href="#settings" data-toggle="tab">Settings</a></li>
					</ul>
					<div class="tab-content">
						<div class="active tab-pane" id="activity">
							<!-- Post -->
							<div class="post">
								<div class="user-block">
									<font class="info"><?=$this->session->flashdata('pesan');?></font>
									<img class="img-circle img-bordered-sm" src="<?= config_item('base_url')?>images/user.png" alt="user image">
									<span class="username">
										<a href="#"><?=$result['nama']?></a>
									</span>
									<span class="description">Level: <?= $result['level'];?></span>
									<hr>
									<div class="row">
										<div class="col-sm-12">
											<div class="form-group">
												<label class="col-sm-2 control-label">Nama</label>
												<label class="col-sm-10 control-label">: <?= $result['nama'] ?></label>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">NIP</label>
												<label class="col-sm-10 control-label">: <?= $result['nip'] ?></label>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Golongan</label>
												<label class="col-sm-10 control-label">: <?= $result['golongan_nama'] ?></label>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">Jabatan</label>
												<label class="col-sm-10 control-label">: <?= $result['jabatan_nama'] ?></label>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">TTL</label>
												<label class="col-sm-10 control-label">: <?= $result['tempat_lahir'].' / '.format_tanggal($result['tanggal_lahir']) ?></label>
											</div>
											<div class="form-group">
												<label class="col-sm-2 control-label">No Rekening</label>
												<label class="col-sm-10 control-label">: <?= $result['no_rekening'] ?></label>
											</div>
										</div>
									</div>
								</div>
								<hr>
							</div>
							<!-- /.post -->
						</div>
						<!-- /.tab-pane -->

						<div class="tab-pane" id="settings">
							<form class="form-horizontal" id="frmbarangmasuk" action="<?=site_url('profils/proses'); ?>" method="post">
								<div class="form-group">
									<label for="inputName" class="col-sm-2 control-label">Username</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="username" id="username" value="<?=$result['username']?>" disabled>
									</div>
								</div>

								<div class="form-group">
									<label for="inputName" class="col-sm-2 control-label">Nama Lengkap</label>
									<div class="col-sm-10">
										<input type="text" class="form-control" name="nama" id="nama" value="<?=$result['nama']?>" placeholder="Nama Lengkap...">
									</div>
								</div>

								<div class="form-group">
									<label for="inputName" class="col-sm-2 control-label">Tempat Tanggal Lahir</label>
									<div class="col-sm-5">
										<input type="text" class="form-control" id="tempat_lahir" name="tempat_lahir" value="<?=$result['tempat_lahir']?>" placeholder="Masukan Tempat Lahir Pegawai" required>
									</div>
									<div class="col-sm-5">
										<input type="text" class="form-control pull-right" id="datepicker" name="tanggal_lahir" readonly="readonly" value="<?=explode_tanggal($result['tanggal_lahir']) ?>">
										<font color="red" class="tanggal_lahir"></font>
									</div>
								</div>

								<div class="form-group">
									<label for="nama" class="col-sm-2 control-label">Alamat </label>
									<div class="col-sm-10">
										<textarea class="form-control" col="2" name="alamat" id="alamat" placeholder="Masukan Alamat Pegawai"><?= $result['alamat']; ?></textarea>
									</div>
								</div>

								<div class="form-group">
									<label for="no_rekening" class="col-sm-2 control-label">No Rekening <font color="red">*</font></label>
									<div class="col-sm-10">
										<input type="text" class="form-control" id="no_rekening" name="no_rekening" value="<?= $result['no_rekening'] ?>" placeholder="Masukan Nomor Rekening Pegawai" required>
									</div>
								</div>

								<div class="form-group">
									<label for="inputSkills" class="col-sm-2 control-label">Password</label>
									<div class="col-sm-10">
										<input type="password" class="form-control" name="password" id="password" placeholder="Password...">
										<span style="color:red;">isikan password, jika ingin menggantikan password<span>
									</div>
								</div>

								<div class="form-group">
									<div class="col-sm-offset-2 col-sm-10">
										<button type="submit" class="btn btn-danger">Submit</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /.tab-pane -->
					</div>
					<!-- /.tab-content -->
				</div>
				<!-- /.nav-tabs-custom -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>