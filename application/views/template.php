<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Aplikasi Penggajian TVRI</title>
	<link rel="shortcut icon" href="<?= config_item('base_url') ?>images/favicon.ico"/>
	<!-- Tell the browser to be responsive to screen width -->
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<!-- Bootstrap 3.3.7 -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/bootstrap/dist/css/bootstrap.min.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/font-awesome/css/font-awesome.min.css">
	<!-- Ionicons -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/Ionicons/css/ionicons.min.css">
	<!-- DataTables -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
	<!-- Theme style -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/dist/css/AdminLTE.min.css">
	<!-- AdminLTE Skins. Choose a skin from the css/skins
	folder instead of downloading all of them to reduce the load. -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/dist/css/skins/_all-skins.min.css">
	<!-- Date Picker -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
	<!-- Daterange picker -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/bower_components/bootstrap-daterangepicker/daterangepicker.css">
	<!-- bootstrap wysihtml5 - text editor -->
	<link rel="stylesheet" href="<?= config_item('base_url')?>theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">

	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
	<?= isset($_styles) ? $_styles : ""; ?>
</head>
<?php 
	$url_1 = $this->uri->segment('1');
?>
<body class="hold-transition skin-blue sidebar-mini">
	<div class="wrapper">

		<header class="main-header">
			<!-- Logo -->
			<a href="<?= site_url()?>" class="logo">
				<!-- mini logo for sidebar mini 50x50 pixels -->
				<span class="logo-mini"><b>A</b>P</span>
				<!-- logo for regular state and mobile devices -->
				<span class="logo-lg"><b>Aplikasi</b> Penggajian</span>
			</a>
			<!-- Header Navbar: style can be found in header.less -->
			<nav class="navbar navbar-static-top">
				<!-- Sidebar toggle button-->
				<a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
					<span class="sr-only">Toggle navigation</span>
				</a>

				<div class="navbar-custom-menu">
					<ul class="nav navbar-nav">
						<li class="dropdown user user-menu">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown">
								<img src="<?= config_item('base_url')?>images/user.png" class="user-image" alt="User Image">
								<span class="hidden-xs"><?= $this->session->userdata('username') ?></span>
							</a>
							<ul class="dropdown-menu">
								<!-- User image -->
								<li class="user-header">
								<img src="<?= config_item('base_url')?>images/user.png" class="img-circle" alt="User Image">
								<p>
									<?= $this->session->userdata('username').
									"<small>".$this->session->userdata('nama_lengkap')."</small>"
									?>
								</p>
								</li>
								<!-- Menu Footer-->
								<li class="user-footer">
									<div class="pull-left">
										<a href="<?= site_url('profils');?>" class="btn btn-default btn-flat">Profile</a>
									</div>
									<div class="pull-right">
										<a href="<?= site_url('logins/logout');?>" class="btn btn-default btn-flat">Sign out</a>
									</div>
								</li>
							</ul>
						</li>
					</ul>
				</div>
			</nav>
		</header>
		<!-- Left side column. contains the logo and sidebar -->
		<aside class="main-sidebar">
			<!-- sidebar: style can be found in sidebar.less -->
			<section class="sidebar">
				<!-- Sidebar user panel -->
				<div class="user-panel">
					<div class="pull-left image">
						<img src="<?= config_item('base_url')?>images/user.png" class="img-circle" alt="User Image">
					</div>
				</div>
				
				<!-- /.search form -->
				<!-- sidebar menu: : style can be found in sidebar.less -->
				<ul class="sidebar-menu" data-widget="tree">
					<li class="<?= $url_1 == "" ? "active" : ""; ?>"><a href="<?= site_url();?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>	

					<li class="<?= $url_1 == "users" ? "active" : ""; ?>"><a href="<?= site_url('users');?>"><i class="fa fa-users"></i> <span>Pegawai</span></a></li>

					<li class="<?= $url_1 == "pagus" ? "active" : ""; ?>"><a href="<?= site_url('pagus');?>"><i class="fa fa-money"></i> <span>Pagu Belanja</span></a></li>

					<li class="treeview<?= $url_1 == "settings" || $url_1 == "bidangs" || $url_1 == "satuan_barangs" || $url_1 == "user_logins" ? " active" : ""; ?>">
						<a href="#">
							<i class="fa fa-gears"></i> <span>Setting</span>
							<span class="pull-right-container">
								<i class="fa fa-angle-left pull-right"></i>
							</span>
						</a>
						<ul class="treeview-menu">
							<li class="<?= $url_1 == "settings" ? "active" : ""; ?>"><a href="<?= site_url('settings');?>"><i class="fa fa-toggle-right"></i> Setting Utama </a></li>

							<li class="<?= $url_1 == "user_logins" ? "active" : ""; ?>"><a href="<?= site_url('user_logins');?>"><i class="fa fa-toggle-right"></i> Pegawai Login </a></li>

							<li class="<?= $url_1 == "bidangs" ? "active" : ""; ?>"><a href="<?= site_url('bidangs');?>"><i class="fa fa-toggle-right"></i> Bidang </a></li>

							<li class="<?= $url_1 == "satuan_barangs" ? "active" : ""; ?>"><a href="<?= site_url('satuan_barangs');?>"><i class="fa fa-toggle-right"></i> Satuan </a></li>
						</ul>
					</li>
					
				</ul>
			</section>
			<!-- /.sidebar -->
		</aside>

		<!-- Content Wrapper. Contains page content -->
		<?= $contents; ?>
		<!-- /.content-wrapper -->

		<footer class="main-footer">
			<div class="pull-right hidden-xs">
				<b>Version</b> 0.0.1
			</div>
			<strong>Copyright &copy; <?= date('Y')?> tvri.</strong> All rights reserved.
		</footer>
	</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?= config_item('base_url')?>theme/bower_components/jquery/dist/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?= config_item('base_url')?>theme/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= config_item('base_url')?>theme/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?= config_item('base_url')?>theme/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?= config_item('base_url')?>theme/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Morris.js charts -->
<script src="<?= config_item('base_url')?>theme/bower_components/raphael/raphael.min.js"></script>
<script src="<?= config_item('base_url')?>theme/bower_components/morris.js/morris.min.js"></script>
<!-- Sparkline -->
<script src="<?= config_item('base_url')?>theme/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?= config_item('base_url')?>theme/bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?= config_item('base_url')?>theme/bower_components/moment/min/moment.min.js"></script>
<script src="<?= config_item('base_url')?>theme/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- datepicker -->
<script src="<?= config_item('base_url')?>theme/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?= config_item('base_url')?>theme/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?= config_item('base_url')?>theme/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?= config_item('base_url')?>theme/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?= config_item('base_url')?>theme/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?= config_item('base_url')?>theme/dist/js/demo.js"></script>
<script src="<?= config_item('base_url')?>theme/dist/js/jquery.maskMoney.min.js"></script>
<script>
	var base_url = "<?= config_item('base_url')?>";
	$(function () {
		$('#example1').DataTable()
		$('#example2').DataTable({
			'paging'      : true,
			'lengthChange': false,
			'searching'   : false,
			'ordering'    : true,
			'info'        : true,
			'autoWidth'   : false
		})
		
		//Date picker
		$('#datepicker').datepicker({
			orientation: "bottom auto",
			autoclose: true,
			format: 'dd-mm-yyyy'
		})
		
		$('.separator_uang').maskMoney({precision:0});
	});
	
	function getkey(e){
		if (window.event)
		return window.event.keyCode;
		else if (e)
		return e.which;
		else
		return null;
	}
	function goodchars(e, goods, field){
		var key, keychar;
		key = getkey(e);
		if (key == null) return true;

			keychar = String.fromCharCode(key);
			keychar = keychar.toLowerCase();
			goods = goods.toLowerCase();

		// check goodkeys
		if (goods.indexOf(keychar) != -1)
			return true;
		// control keys
		if ( key==null || key==0 || key==8 || key==9 || key==27 )
			return true;

		if (key == 13) {
			var i;
			for (i = 0; i < field.form.elements.length; i++)
			if (field == field.form.elements[i])
				break;
			i = (i + 1) % field.form.elements.length;
			field.form.elements[i].focus();
			return false;
		};
		// else return false
		return false;
	}
</script>
<?= isset($_scripts) ? $_scripts : ""; ?>
</body>
</html>