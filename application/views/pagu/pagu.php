<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Daftar Pagu Belanja
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url();?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li>Pagu Belanja</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<!-- SELECT2 EXAMPLE -->
	<div class="box box-default">
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="box-header with-border col-md-2">
					<button type="button" class="btn btn-primary tambah" data-toggle="modal" data-target="#modal-default" akunid="0" tingkat="0">
						Tambah
					</button>
				</div>
				<div class="clearfix"></div>
				<div class="col-md-12">
					<font class="info"><?=$this->session->flashdata('pesan');?></font>


					<table class="table table-bordered">
						<tbody>
							<tr>
								<th style="width: 10px">No</th>
								<th>Nama</th>
								<th style="width: 40px">Action</th>
							</tr>
							<?php foreach ($result as $key => $val) { ?>
								<tr>
									<td>1.</td>
									<td></td>
									<td><span class="badge bg-red">+</span></td>
								</tr>
							<?php } ?>
						</tbody>
					</table>

				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			Informasi seluruh Daftar Pagu Belanja.
		</div>
	</div>

	<div class="modal fade" id="modal-default">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title">Default Modal</h4>
				</div>
				<form role="form" action="<?=site_url('pagus/proses'); ?>" method="post">
					<div class="modal-body">
						<div class="form-group">
							<label for="nama">Nama Pagu <font color="red">*</font></label>
							<input type="text" class="form-control" id="nama" name="nama" placeholder="Masukan Pagu Belanja" required>
							<input type="hidden" class="form-control" id="paguid" name="paguid" value="">
							<input type="hidden" class="form-control" id="tingkat" name="tingkat" value="">
						</div>
						<div class="form-group">
							<label for="nama">Kode Akun <font color="red">*</font></label>
							<input type="text" class="form-control" id="kode" name="kode" placeholder="Masukan Kode Akun" required>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
						<button type="submit" class="btn btn-primary">Save</button>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.box -->
	</section>
</div>


<script src="<?php echo base_url();?>asset/js/jquery-2.1.4.min.js"></script>
<script type="text/javascript">
    var base_url = "<?= config_item('base_url')?>";
    jQuery(function($) {
    	$(".tambah").click(function(){
    		var akunid 	= $(this).attr('akunid');
    		var tingkat = $(this).attr('tingkat');

    		$("#paguid").val(akunid);
    		$("#tingkat").val(tingkat);
    	});
        /*$(".btn_tambah_saudara").click(function(){
            if($("#saudara_id").val() == ""){
                alert('Pilih terlebih dahulu');
            } else {
                $(".btn_tambah_saudara").attr("disabled", true);
                $(".btn_tambah_saudara").html("Sedang Proses...");

                var siswa_id    = $("#siswa_id").val();
                var siswa_uuid  = $("#siswa_uuid").val();
                var saudara_id  = $("#saudara_id").val();
                $.ajax({
                    url: base_url + "data_siswa/siswa/step3_tambah_saudara",
                    type: "POST",
                    data: { 
                        siswa_uuid  : siswa_uuid,
                        siswa_id    : siswa_id,
                        saudara_id  : saudara_id,
                        keterangan  : "saudara"
                    },
                    dataType: "json",
                    cache: false,
                    success: function(response){
                        window.location.assign(response.url);
                    }
                });
            }
        });*/

        /*$(".btn_tambah").click(function(){
            $(".btn_tambah").attr("disabled", true);
            $(".btn_tambah").html("Sedang Proses...");
            if($("#siswa_saudara_nama").val() == "") {
                alert("Silahkah Masukan Nama Saudara...");
                $("#siswa_saudara_nama").focus();
                $(".btn_tambah").removeAttr("disabled");
                $(".btn_tambah").html("Tambah");
                return false;
            } else if($("#siswa_saudara_jenis_kelamin").val() == "") {
                alert("Silahkah Masukan Jenis Kelamin Saudara Anda...");
                $("#siswa_saudara_jenis_kelamin").focus();
                $(".btn_tambah").removeAttr("disabled");
                $(".btn_tambah").html("Tambah");
                return false;
            } else if($("#siswa_saudara_tanggal_lahir").val() == "") {
                alert("Silahkah Masukan Tanggal Lahir Saudara Anda...");
                $("#siswa_saudara_tanggal_lahir").focus();
                $(".btn_tambah").removeAttr("disabled");
                $(".btn_tambah").html("Tambah");
                return false;
            } else if($("#siswa_saudara_pekerjaan").val() == "") {
                alert("Silahkah Masukan Pekerjaan Saudara Anda...");
                $("#siswa_saudara_pekerjaan").focus();
                $(".btn_tambah").removeAttr("disabled");
                $(".btn_tambah").html("Tambah");
                return false;
            } else {
                var a = $("#siswa_id").val();
                var b = $("#siswa_saudara_nama").val();
                var c = $("#siswa_saudara_jenis_kelamin").val();
                var d = $("#siswa_saudara_tanggal_lahir").val();
                var e = $("#siswa_saudara_pendidikan_terakhir").val();
                var f = $("#siswa_saudara_pekerjaan").val();
                $.ajax({
                    url: base_url + "data_siswa/siswa/step3_submit",
                    type: "POST",
                    data: { 
                        siswa_id                            : a,
                        siswa_saudara_nama                  : b,
                        siswa_saudara_jenis_kelamin         : c,
                        siswa_saudara_tanggal_lahir         : d,
                        siswa_saudara_pendidikan_terakhir   : e,
                        siswa_saudara_pekerjaan             : f,
                    },
                    dataType: "html",
                    cache: false,
                    success: function(response){
                        $(".help_step3").append(response).trigger('chosen:updated');
                        $(".btn_tambah").removeAttr("disabled");
                        $(".btn_tambah").html("Tambah");
                        $("#siswa_saudara_nama").val('');
                        $("#siswa_saudara_jenis_kelamin")[0].selectedIndex = "";
                        $("#siswa_saudara_tanggal_lahir").val('');
                        $("#siswa_saudara_pendidikan_terakhir")[0].selectedIndex = 0;
                        $("#siswa_saudara_pekerjaan").val('');
                    }
                });
            }
        });*/
    });

    /*function hapus_saudara(value, ket = ""){
        var conf = confirm("Yakin Ingin Hapus ?");
        if(conf){
            var hasil = $.ajax({
                url: base_url + "data_siswa/siswa/step3_hapus",
                type: "POST",
                global: true,
                data: {
                    uuid: value,
                    ket : ket
                },
                dataType: "html",
                async: false
            }).responseText;
            if(hasil == 'berhasil'){
                $(".uuid_"+value).remove();
            }
        }
    }*/
</script>