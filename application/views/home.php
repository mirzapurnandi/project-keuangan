<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Dashboard <small>Control panel</small></h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
		<div class="row">

			<div class="col-md-8">
				<div class="box box-default">
					<div class="box-header with-border">
						<i class="fa fa-bullhorn"></i>

						<h3 class="box-title">BNN Provinsi Aceh</h3>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<div class="callout callout-danger">
							<h4>NAMA PEJABAT PEMBUAT KOMITMEN</h4>

							<p><?= $pejabat_komitmen->nama.'<br>'.$pejabat_komitmen->nip; ?></p>
						</div>
						<div class="callout callout-info">
							<h4>NAMA BENDAHARA</h4>

							<p><?= $bendahara->nama.'<br>'.$bendahara->nip; ?></p>
						</div>
						<div class="callout callout-warning">
							<h4>NAMA PENERIMA BARANG</h4>

							<p><?= $penerima_barang->nama.'<br>'.$penerima_barang->nip; ?></p>
						</div>
						<div class="callout callout-success">
							<h4>STAF SUBBAG SARPRAS</h4>

							<p><?= $subbag_sarpras->nama.'<br>'.$subbag_sarpras->nip; ?></p>
						</div>
						<div class="callout bg-maroon">
							<h4>KABAG UMUM</h4>

							<p><?= $kabag_umum->nama.'<br>'.$kabag_umum->nip; ?></p>
						</div>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
		</div>
		<!-- /.row -->
	</section>
	<!-- /.content -->
</div>