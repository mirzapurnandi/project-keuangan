<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>
			Daftar Bidang
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?= site_url();?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li>Bidang</li>
		</ol>
	</section>

	<!-- Main content -->
	<section class="content">
	<!-- SELECT2 EXAMPLE -->
	<div class="box box-default">
		<?php 
		$show = $result_edit['id'] == "" ? "Tambah" : "Edit";
		if($this->session->userdata('level') == "admin"){ ?>
		<div class="box-header with-border col-md-12">
			<h1><?=$show?> Data</h1>
			<form class="form-horizontal" id="frmbidang" action="<?=site_url('bidangs/proses'); ?>" method="post">
				<input type="hidden" name="bidangid" value="<?=$result_edit['id']?>">
				<div class="box-body">
					<div class="form-group">
						<label for="nominaldana" class="col-sm-2 control-label">Bidang <font color="red">*</font></label>
						<div class="col-sm-10">
							<input type="text" class="form-control" name="nama" id="nama" value="<?=$result_edit['nama']?>" placeholder="Nama Bidang" required>
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
							<button type="submit" class="btn btn-info pull-left"><?=$show?></button>
						</div>
					</div>
				</div>
			</form>
		</div>
		<?php } ?>
		<!-- /.box-header -->
		<div class="box-body">
			<div class="row">
				<div class="col-md-12">
					<font class="info"><?=$this->session->flashdata('pesan');?></font>
					<table id="example1" class="table table-bordered table-striped">
						<thead>
						<tr>
							<th width="10%">No</th>
							<th width="70%">Nama Bidang</th>
							<th width="20%">Actions</th>
						</tr>
						</thead>
						<tbody>
						<?php 
						if(count($result) > 0){
						foreach($result as $key => $val){ ?>
						<tr>
							<td><?= $key + 1; ?></td>
							<td><?= $val['nama'] ?></td>
							<?php if($this->session->userdata('level') == "admin"){ ?>
							<td>
								<a href="<?= site_url('bidangs/index/'.$val['id']); ?>" class="btn btn-info btn-xs"><span class="fa fa-pencil"></span> Edit</a> 
								<a href="<?= site_url('bidangs/remove/'.$val['id']); ?>" class="btn btn-danger btn-xs" onclick="return confirm('Mau Menghapus Data ini... ?')"><span class="fa fa-trash"></span> Delete</a>
							</td>
							<?php } ?>
						</tr>
						<?php } }?>
						</tbody>
					</table>
				</div>
			</div>
			<!-- /.row -->
		</div>
		<!-- /.box-body -->
		<div class="box-footer">
			Informasi seluruh Daftar Bidang.
		</div>
	</div>
	<!-- /.box -->
	</section>
</div>