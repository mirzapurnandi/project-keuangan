<div class="content-wrapper">
	<!-- Content Header (Page header) -->
	<section class="content-header">
		<h1>Setting</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Setting</li>
		</ol>
	</section>

	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-info">
					<div class="box-header with-border">
						<h3 class="box-title">Setting Data</h3>
					</div>
					<font class="info"><?=$this->session->flashdata('pesan');?></font>
					<form role="form" action="<?=site_url('settings/proses'); ?>" method="post" enctype="multipart/form-data">
						<div class="modal-body">
							<div class="box-body">
								<div class="form-group">
									<label for="nama">Nama Pejabat Pembuat Komitmen</label>
									<?= form_dropdown('pejabat_komitmen', $user, $result['pejabat_komitmen'], 'id="pejabat_komitmen" class="form-control"'); ?>
								</div>
								<div class="form-group">
									<label for="nama">Nama Bendahara</label>
									<?= form_dropdown('bendahara', $user, $result['bendahara'], 'id="bendahara" class="form-control"'); ?>
								</div>
								<div class="form-group">
									<label for="nama">Nama Penerima Barang</label>
									<?= form_dropdown('penerima_barang', $user, $result['penerima_barang'], 'id="penerima_barang" class="form-control"'); ?>
								</div>
								<div class="form-group">
									<label for="nama">Staf Subbag Sarpras</label>
									<?= form_dropdown('subbag_sarpras', $user, $result['subbag_sarpras'], 'id="subbag_sarpras" class="form-control"'); ?>
								</div>
								<div class="form-group">
									<label for="nama">Kabag Umum</label>
									<?= form_dropdown('kabag_umum', $user, $result['kabag_umum'], 'id="kabag_umum" class="form-control"'); ?>
								</div>
								<div class="form-group">
									<label for="kop_surat">Kop Surat</label>
									<input type="file" id="kop_surat" name="kop_surat">
									<img style='width: 100%;' src='<?= config_item('img_dir')."kop_surat/".$result['kop_surat'] ?>'>
								</div>
							</div>
						</div>
						<div class="modal-footer">
							<button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-primary">Simpan</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>