<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

if (!function_exists('dropdown_user')) {
	function dropdown_user(){
		$CI = &get_instance();
		$CI->load->database();
		## Menampilkan data
		$CI->db->select('*');
		$CI->db->from('user');
		$CI->db->order_by('id', 'asc');
		$hasil = $CI->db->get();

		$arr_data[''] = "== Pilih ==";
		if($hasil->num_rows()>0) {
			foreach($hasil->result_array() as $key => $val){
				$arr_data[$val['id']] = $val['nama'].' ('.$val['nip'].')';
			}
		}
		return $arr_data;
	}
}

if (!function_exists('dropdown_umum')) {
	function dropdown_umum($table, $kata){
		$CI = &get_instance();
		$CI->load->database();
		## Menampilkan data
		$CI->db->select('*');
		$CI->db->from($table);
		$CI->db->order_by('id', 'asc');
		$hasil = $CI->db->get();

		$arr_data[''] = $kata;
		if($hasil->num_rows()>0) {
			foreach($hasil->result_array() as $key => $val){
				$arr_data[$val['id']] = $val['nama'];
			}
		}
		return $arr_data;
	}
}

if (!function_exists('dropdown_username')) {
	function dropdown_username($userid = ""){
		$CI = &get_instance();
		$CI->load->database();
		## Menampilkan data
		$CI->db->select('*');
		$CI->db->from('user');
		$CI->db->order_by('user.id', 'asc');
		$hasil = $CI->db->get();

		$arr_data[''] = "== Pilih Pegawai ==";
		if($hasil->num_rows()>0) {
			foreach($hasil->result_array() as $key => $val){
				$arr_data[$val['id']] = $val['nama'];
			}
		}
		return ($arr_data);
	}
}
?>
