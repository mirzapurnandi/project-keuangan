<?php
	if(!defined('BASEPATH'))
		exit('No direct access allowed !');
		
	class Ajax_function extends CI_Controller{
		function __construct(){
			parent::__construct();
			//cek_session();
			$this->load->model('umum');
		}

		function cari_checkout(){
			$checkin		= $_POST['tgl_checkin'];
			$jumlah_malam	= $_POST['jumlah_malam'];

			## Mencari Data ##
			$tanggal 	= strtotime($checkin . '+' . $jumlah_malam . ' day');
			## Akhir Mencari data ##

			$arr_data = array(
				'tgl_checkout_view'	=> date("d-m-Y", $tanggal),
				'tgl_checkout'		=> date("Y-m-d", $tanggal)
			);
			echo json_encode($arr_data);
		}

		function get_kamar(){
			$jenis_kamar	= $_POST['jenis_kamar'];
			$tgl_checkin	= $_POST['tgl_checkin'];
			$jumlah_malam	= $_POST['jumlah_malam'];
			$trbo			= isset($_POST['trbo']) ? $_POST['trbo'] : "";
			$noid			= isset($_POST['noid']) ? $_POST['noid'] : "";

			$kamar			= dropdown_kamar("", $jenis_kamar, "pilihan", "semua");

			## mencari array dari kamar yang sudah dipakai##
			$arr_kamar = array(); 
			for ($i=0; $i < $jumlah_malam; $i++) { 
				$tanggal 	= strtotime($tgl_checkin. '+' .$i. ' day');
				$show_tgl	= date("Y-m-d", $tanggal);
				$jumlah 	= $this->umum->cek_kamar_kosong($jenis_kamar, $show_tgl, $trbo, $noid);
				
				if(count($jumlah) > 0){
					$arr_kamar += $jumlah;
				}
			}
			## akhir ##

			$html = '<select name="kamar" id="kamar" class="form-control kamarid" required>';
			foreach ($kamar as $keys => $vals) {
				$disabled = "";
				$catatan = "";
				if(in_array($keys, $arr_kamar)){
					$disabled 	= "disabled='disabled'";
					$catatan 	= " Kamar ini sudah digunakan ";
				}
				$html .= '<option value="'.$keys.'" '.$disabled.'>'.$vals.$catatan.'</option>';
			}
			$html .= '</select>'; 
			$arr_data = array(
				'kamar'	=> $html
			);
			echo json_encode($arr_data);
		}

		function get_kamar_order(){
			$tgl_checkin	= $_POST['tgl_checkin'];
			$jumlah_malam	= $_POST['jumlah_malam'];

			$kamar			= dropdown_kamar("", "", "pilihan", "semua");

			## mencari array dari kamar yang sudah dipakai##
			$arr_kamar = array(); 
			for ($i=0; $i < $jumlah_malam; $i++) { 
				$tanggal 	= strtotime($tgl_checkin. '+' .$i. ' day');
				$show_tgl	= date("Y-m-d", $tanggal);
				$jumlah 	= $this->umum->cek_kamar_kosong("", $show_tgl);
				
				if(count($jumlah) > 0){
					$arr_kamar += $jumlah;
				}
			}
			## akhir ##

			$html = '';
			foreach ($kamar as $keys => $vals) {
				$disabled = "";
				$catatan = "";
				if(in_array($keys, $arr_kamar)){
					$disabled 	= "disabled='disabled'";
					$catatan 	= " Kamar ini sudah digunakan ";
				}
				$html .= '<option value="'.$keys.'" '.$disabled.'>'.$vals.$catatan.'</option>';
			}
			echo $html;
		}

		function cari_reservasis(){
			$tanggal		= $_POST['tanggal'];
			$tgl_skrg 		= date("Y-m-d");

			$result = $this->umum->cek_reservasi($tanggal);

			$html = "";
			if(count($result) > 0){
				$html .= '<table class="table table-striped">
			                <tbody>
				                <tr>
				                  <th>No</th>
				                  <th>Guest Name</th>
				                  <th>Room</th>
				                  <th>Room Type</th>
				                  <th>CheckIn / CheckOut</th>
				                  <th>Lose</th>
				                  <th>Keterangan</th>
				                  <th>Status</th>
				                </tr>';

				
					foreach ($result as $key => $val) {
						$no = $key + 1;
						if($val['checkin'] == $tgl_skrg){
							$status = '<a href="'.config_item('base_url').'checkins" class="label label-success"> <i class="fa fa-fw fa-calendar-check-o"></i>  Check-In >></a>';
						} elseif($val['checkout'] == $tgl_skrg){
							$status = '<a href="'.config_item('base_url').'checkouts" class="label label-danger"> <i class="fa fa-fw fa-calendar-times-o"></i> Check-Out <<</a>';
						} else {
							$status = '<span class="label label-warning"> <i class="fa fa-fw fa-bed"></i> Menginap</span>';
						}
						$html .= '<tr>
				                  <td>'.$no.'</td>
				                  <td>'.$val["nama_customer"].'</td>
				                  <td>UBH-'.$val["kamarid"].'</td>
				                  <td>'.$val["nama_kamar"].'</td>
				                  <td><i class="fa fa-fw fa-calendar-check-o" title="Check-In"></i> '.format_tanggal($val["checkin"], "bulan").'<br><i class="fa fa-fw fa-calendar-times-o" title="Check-Out"></i> '.format_tanggal($val['checkout'], "bulan").'</td>
				                  <td>'.$val["jumlah_malam"].' Malam</td>
				                  <td>'.$val["nama_mitra"].'</td>
				                  <td>'.$status.'</td>
				                </tr>';
					}
				

				$html .= '</tbody></table>';
			} else {
				$html .= "<h2>Tidak ada Reservasi</h2>";
			}

			$arr_data = array(
					'html' => $html
				);

			echo json_encode($arr_data);

		}
	}
?>