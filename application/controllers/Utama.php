<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Utama extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session();
			$this->load->model(array('user', 'setting'));
		}

		function index(){
			$setting = $this->setting->get_data();

			$data['pejabat_komitmen'] 	= $this->user->utama($setting['pejabat_komitmen']);
			$data['bendahara'] 			= $this->user->utama($setting['bendahara']);
			$data['penerima_barang'] 	= $this->user->utama($setting['penerima_barang']);
			$data['subbag_sarpras'] 	= $this->user->utama($setting['subbag_sarpras']);
			$data['kabag_umum'] 		= $this->user->utama($setting['kabag_umum']);

			$this->template->view('template', 'home', $data);
		}
	}
?>