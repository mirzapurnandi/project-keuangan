<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Profils extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session();
			$this->load->model('user');
		}

		function index(){
			$userid = $this->session->userdata('userid');
			$result = $this->user->get_data($userid);
			$data['result']	= $result;

			$this->template->view('template', 'profil', $data);
		}
		
		function proses(){
			$this->db->trans_start();
			$userid = $this->session->userdata('userid');
			$return_data = array(
				'nama'			=> $this->input->post('nama'),
				'tempat_lahir'	=> $this->input->post('tempat_lahir'),
				'tanggal_lahir'	=> explode_tanggal_datepicker($this->input->post('tanggal_lahir')),
				'alamat'		=> $this->input->post('alamat'),
				'no_rekening'	=> $this->input->post('no_rekening')
			);
			if($this->input->post('password') != ""){
				$return_data += array('password' => md5($this->input->post('password')));
			}

			$this->global_model->update('user', $return_data, array('id' => $userid));
			$this->db->trans_complete();
			$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah diperbaharui</div>");
			redirect('profils/');
		}
	}
?>