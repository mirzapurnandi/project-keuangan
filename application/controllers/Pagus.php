<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Pagus extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session();
			$this->load->model(array('pagu'));
		}

		function index(){
			$data['result'] = $this->pagu->get_data();

			$this->template->view('template', 'pagu/pagu', $data);
		}

		function proses(){
			$this->db->trans_start();
			$bidangid = empty($this->input->post('bidangid')) ? "" : $this->input->post('bidangid');

			$paguid = $this->input->post('paguid');
			$nama 	= $this->input->post('nama');
			$kode 	= $this->input->post('kode');
			$tingkat = $this->input->post('tingkat');

			$cek_data = $this->pagu->cek_data($kode);
			if($cek_data > 0){
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-danger' role='alert'>Kode Akun ".$kode." sudah tersedia</div>");
				redirect('pagus/');
				die();
			}

			$return_data = array(
				'nama' 		=> $nama,
				'kode'		=> $kode,
				'akunid'	=> $paguid,
				'tingkat'	=> $tingkat,
				'tahun'		=> '2020',
				'created'	=> date("Y-m-d H:i:s", time())
			);

			$this->global_model->insert('pagu', $return_data);
			$this->db->trans_complete();
			$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
			redirect('pagus/');
		}
	}
?>