<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Users extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session();
			$this->load->model('user');
		}

		function index(){
			$data['result'] = $this->user->get_data();

			$this->template->view('template', 'user/user', $data);
		}

		function add(){
			$data['bidang']			= dropdown_umum('bidang', "== Pilih Bidang ==");
			$data['jenis_kelamin']	= config_item('jenis_kelamin');

			$this->template->view('template', 'user/user_add', $data);
		}

		function edit($id){
			$data['userid']			= $id;
			$data['bidang']			= dropdown_umum('bidang', "== Pilih Bidang ==");
			$data['jenis_kelamin']	= config_item('jenis_kelamin');

			$result	= $this->user->get_data($id);
			$data['result']	= $result;

			$this->template->view('template', 'user/user_edit', $data);
		}

		function proses(){
			$this->db->trans_start();
			$userid			= empty($this->input->post('userid')) ? "" : $this->input->post('userid');
			$nama			= $this->input->post('nama');
			$nip			= $this->input->post('nip');
			$bidang			= $this->input->post('bidang');
			$no_hp			= $this->input->post('no_hp');
			$jenis_kelamin	= $this->input->post('jenis_kelamin');

			$return_data = array(
				'nama'				=> $nama,
				'nip'				=> $nip,
				'bidangid'			=> $bidang,
				'no_hp'				=> $no_hp,
				'jk'				=> $jenis_kelamin
			);

			if($userid == ""){
				$return_data += array(
					'uuid'		=> UUID::v5(mt_rand(0, 0xffff).microtime().time()),
					'created'	=> date("Y-m-d H:i:s", time())
				);
				$this->global_model->insert('user', $return_data);
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
			} else {
				$this->global_model->update('user', $return_data, array('id' => $userid));
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			}
			redirect('users/');
		}

		function remove($id){
			$this->global_model->delete('user', array('id' => $id));
			$this->session->set_flashdata('pesan', '<div class="form-group"><div class="col-sm-12 alert alert-error" role="alert">Data Sudah di Hapus</div></div>');
			echo "<script>window.history.go(-1);</script>";
		}
	}
?>