<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Settings extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session();
			$this->load->model('setting');
		}

		function index(){
			$data['user'] 	= dropdown_user();
			$data['result'] = $this->setting->get_data();

			$this->template->view('template', 'setting', $data);
		}

		function proses(){
			$this->db->trans_start();
			$pejabat_komitmen	= $this->input->post('pejabat_komitmen');
			$bendahara			= $this->input->post('bendahara');
			$penerima_barang	= $this->input->post('penerima_barang');
			$subbag_sarpras		= $this->input->post('subbag_sarpras');
			$kabag_umum			= $this->input->post('kabag_umum');

			$return_data = array(
				'pejabat_komitmen' 	=> $pejabat_komitmen,
				'bendahara'			=> $bendahara,
				'penerima_barang'	=> $penerima_barang,
				'subbag_sarpras'	=> $subbag_sarpras,
				'kabag_umum'		=> $kabag_umum
			);

			// Upload Gambar
			$config['upload_path']	 = './images/kop_surat/';
			$config['allowed_types'] = 'gif|jpg|png|jpeg';
			$config['file_name']	 = time();
			$this->upload->initialize($config);
			$kop_surat = NULL;
			if ($this->upload->do_upload('kop_surat')){
				$kop_surat = $this->upload->data('file_name');
			}
			$return_data += array(
				'kop_surat'	=> $kop_surat
			);

			$this->global_model->update('setting', $return_data, array('id' => config_item('set_id')));
			$this->db->trans_complete();
			$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			redirect('settings/');
		}

	}
?>