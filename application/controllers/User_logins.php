<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class User_logins extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session();
			$this->load->model('user');
		}

		function index(){
			$data['result'] = $this->user->get_data_login();

			$this->template->view('template', 'user/user_login', $data);
		}

		function add(){
			$data['user']			= dropdown_username();
			$data['level']			= config_item('level');

			$this->template->view('template', 'user/user_login_add', $data);
		}

		function edit($id){
			$data['user']			= dropdown_username();
			$data['level']			= config_item('level');

			$result	= $this->user->get_data_login($id);
			$data['result']	= $result;

			$this->template->view('template', 'user/user_login_edit', $data);
		}

		function proses(){
			$this->db->trans_start();
			$loginid		= empty($this->input->post('loginid')) ? "" : $this->input->post('loginid');
			$user			= $this->input->post('user');
			$username		= $this->input->post('username');
			$password		= $this->input->post('password');
			$level			= $this->input->post('level');

			$return_data = array(
				'username'		=> $username,
				'password'		=> md5($password),
				'password_hid'	=> $password,
				'level'			=> $level
			);

			if($loginid == ""){
				$return_data += array(
					'userid'	=> $user,
					'created'	=> date("Y-m-d H:i:s", time())
				);
				$this->global_model->insert('login', $return_data);
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
			} else {
				$this->global_model->update('login', $return_data, array('id' => $loginid));
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			}
			redirect('user_logins/');
		}

		function remove($id){
			$this->global_model->delete('login', array('id' => $id));
			$this->session->set_flashdata('pesan', '<div class="form-group"><div class="col-sm-12 alert alert-error" role="alert">Data Sudah di Hapus</div></div>');
			echo "<script>window.history.go(-1);</script>";
		}
	}
?>