<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Satuan_barangs extends CI_Controller{
		function __construct(){
			parent::__construct();
			cek_session();
			$this->load->model('satuan_barang');
		}

		function index($id = NULL){
			$data['result'] = $this->satuan_barang->get_data();

			$result = array('id' => '', 'nama' => '');
			if(isset($id)){
				$result	= $this->satuan_barang->get_data($id); 
				//$result = $this->global_model->get('bidang', '*', array('id' => $id), true);
			}
			$data['result_edit']	= $result;
			
			$this->template->view('template', 'satuan_barang', $data);
		}

		function proses(){
			$this->db->trans_start();
			$satuanid = empty($this->input->post('satuanid')) ? "" : $this->input->post('satuanid');

			$nama = $this->input->post('nama');

			if($satuanid == ""){
				$return_data = array(
					'nama'		=> $nama,
					'created'	=> date("Y-m-d H:i:s", time())
				);
				$this->global_model->insert('satuan_barang', $return_data);
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Ditambah</div>");
			} else {
				$this->global_model->update('satuan_barang', array('nama' => $nama), array('id' => $satuanid));
				$this->db->trans_complete();
				$this->session->set_flashdata('pesan', "<div class='alert alert-success' role='alert'>Data Sudah Diperbaharui</div>");
			}
			redirect('satuan_barangs/');
		}

		function remove($id){
			$this->global_model->delete('satuan_barang', array('id' => $id));
			$this->session->set_flashdata('pesan', '<div class="form-group"><div class="col-sm-12 alert alert-error" role="alert">Data Sudah di Hapus</div></div>');
			echo "<script>window.history.go(-1);</script>";
		}
	}
?>