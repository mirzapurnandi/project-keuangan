/*
Navicat MySQL Data Transfer

Source Server         : Localhost
Source Server Version : 50620
Source Host           : localhost:3306
Source Database       : db_ade

Target Server Type    : MYSQL
Target Server Version : 50620
File Encoding         : 65001

Date: 2020-01-23 10:06:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bidang
-- ----------------------------
DROP TABLE IF EXISTS `bidang`;
CREATE TABLE `bidang` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of bidang
-- ----------------------------
INSERT INTO `bidang` VALUES ('1', 'Sarpras', '2020-01-17 01:12:29');
INSERT INTO `bidang` VALUES ('2', 'Umum', '2020-01-17 01:14:49');

-- ----------------------------
-- Table structure for login
-- ----------------------------
DROP TABLE IF EXISTS `login`;
CREATE TABLE `login` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` int(11) DEFAULT NULL,
  `username` varchar(200) DEFAULT NULL,
  `password` varchar(200) DEFAULT NULL,
  `password_hid` varchar(200) DEFAULT NULL,
  `level` varchar(100) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of login
-- ----------------------------
INSERT INTO `login` VALUES ('1', null, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', 'admin', '2020-01-15 02:13:28');
INSERT INTO `login` VALUES ('2', '1', 'pegawai1', '0b96cb1d0dfbcc85f6b57041656abc49', 'pegawai1', 'pegawai', '2020-01-17 02:10:41');

-- ----------------------------
-- Table structure for pagu
-- ----------------------------
DROP TABLE IF EXISTS `pagu`;
CREATE TABLE `pagu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) DEFAULT NULL,
  `kode` varchar(50) DEFAULT NULL,
  `akunid` int(11) DEFAULT NULL,
  `tingkat` int(11) NOT NULL DEFAULT '0',
  `tahun` year(4) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of pagu
-- ----------------------------
INSERT INTO `pagu` VALUES ('1', 'Program Dukungan Managemen dan Pelaksanaan', '01', '0', '0', '2020', '2020-01-18 05:47:04');

-- ----------------------------
-- Table structure for satuan_barang
-- ----------------------------
DROP TABLE IF EXISTS `satuan_barang`;
CREATE TABLE `satuan_barang` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `nama` varchar(20) NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of satuan_barang
-- ----------------------------
INSERT INTO `satuan_barang` VALUES ('1', 'Unit', '2018-07-04 05:00:15');
INSERT INTO `satuan_barang` VALUES ('2', 'Buah', '2018-07-04 05:00:15');
INSERT INTO `satuan_barang` VALUES ('3', 'Pasang', '2018-07-04 05:00:27');
INSERT INTO `satuan_barang` VALUES ('4', 'Lembar', '2018-07-04 05:00:27');
INSERT INTO `satuan_barang` VALUES ('5', 'Keping', '2018-07-04 05:01:33');
INSERT INTO `satuan_barang` VALUES ('6', 'Batang', '2018-07-04 05:01:33');
INSERT INTO `satuan_barang` VALUES ('7', 'Bungkus', '2018-07-04 05:02:35');
INSERT INTO `satuan_barang` VALUES ('8', 'Potong', '2018-07-04 05:02:45');
INSERT INTO `satuan_barang` VALUES ('9', 'Tablet', '2018-07-04 05:02:50');
INSERT INTO `satuan_barang` VALUES ('10', 'Ekor', '2018-07-04 05:02:53');
INSERT INTO `satuan_barang` VALUES ('11', 'Rim', '2018-07-04 05:03:09');
INSERT INTO `satuan_barang` VALUES ('12', 'Karat', '2018-07-04 05:03:09');
INSERT INTO `satuan_barang` VALUES ('13', 'Botol', '2018-07-04 05:03:19');
INSERT INTO `satuan_barang` VALUES ('14', 'Butir', '2018-07-04 05:03:19');
INSERT INTO `satuan_barang` VALUES ('15', 'Rol', '2018-07-04 05:03:29');
INSERT INTO `satuan_barang` VALUES ('16', 'Dus', '2018-07-04 05:03:29');
INSERT INTO `satuan_barang` VALUES ('17', 'Karung', '2018-07-04 05:03:40');
INSERT INTO `satuan_barang` VALUES ('18', 'Roli', '2018-07-04 05:03:40');
INSERT INTO `satuan_barang` VALUES ('19', 'Sak', '2018-07-04 05:03:49');
INSERT INTO `satuan_barang` VALUES ('20', 'Bal', '2018-07-04 05:03:49');
INSERT INTO `satuan_barang` VALUES ('21', 'Kaleng', '2018-07-04 05:03:59');
INSERT INTO `satuan_barang` VALUES ('22', 'Set', '2018-07-04 05:03:59');
INSERT INTO `satuan_barang` VALUES ('23', 'Slop', '2018-07-04 05:04:09');
INSERT INTO `satuan_barang` VALUES ('24', 'Gulung', '2018-07-04 05:04:09');
INSERT INTO `satuan_barang` VALUES ('25', 'Ton', '2018-07-04 05:04:24');
INSERT INTO `satuan_barang` VALUES ('26', 'Kilogram', '2018-07-04 05:04:24');
INSERT INTO `satuan_barang` VALUES ('27', 'Gram', '2018-07-04 05:04:39');
INSERT INTO `satuan_barang` VALUES ('28', 'MiliGram', '2018-07-04 05:04:39');
INSERT INTO `satuan_barang` VALUES ('29', 'Meter', '2018-07-04 05:04:45');
INSERT INTO `satuan_barang` VALUES ('30', 'M2', '2018-07-04 05:04:45');
INSERT INTO `satuan_barang` VALUES ('31', 'M3', '2018-07-04 05:05:01');
INSERT INTO `satuan_barang` VALUES ('32', 'Inchi', '2018-07-04 05:05:01');
INSERT INTO `satuan_barang` VALUES ('33', 'Cc', '2020-01-17 07:23:10');
INSERT INTO `satuan_barang` VALUES ('34', 'Liter', '2018-07-04 05:05:12');

-- ----------------------------
-- Table structure for setting
-- ----------------------------
DROP TABLE IF EXISTS `setting`;
CREATE TABLE `setting` (
  `id` int(11) NOT NULL,
  `tahun` char(4) DEFAULT NULL,
  `kop_surat` varchar(200) DEFAULT NULL,
  `pejabat_komitmen` int(11) DEFAULT NULL,
  `bendahara` int(11) DEFAULT NULL,
  `penerima_barang` int(11) DEFAULT NULL,
  `subbag_sarpras` int(11) DEFAULT NULL,
  `kabag_umum` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of setting
-- ----------------------------
INSERT INTO `setting` VALUES ('1', '2020', '1579306004.png', '1', '2', '1', '2', '1');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(200) DEFAULT NULL,
  `nip` varchar(100) DEFAULT NULL,
  `bidangid` int(11) DEFAULT NULL,
  `no_hp` varchar(50) DEFAULT NULL,
  `jk` varchar(255) DEFAULT NULL,
  `uuid` varchar(200) DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'Pegawai 1', '123124312', '1', '0821821821', 'l', '0f32c770-785b-5e38-9009-370e16dcffba', '2020-01-17 01:37:53');
INSERT INTO `user` VALUES ('2', 'Pegawai 2', '324234232', '2', '0345834843543', 'p', '31feb45d-d215-56fb-8634-b2349d8a1596', '2020-01-17 01:46:38');
